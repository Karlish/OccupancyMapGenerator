from PIL import Image, ImageDraw
import numpy as np


class GridPoint:
    def __init__(self, x0, stepX, y0, stepY):
        self.stepX = stepX
        self.stepY = stepY
        self.xStart = x0 * stepX
        self.yStart = y0 * stepY
        self.xEnd = (x0 + 1) * stepX
        self.yEnd = (y0 + 1) * stepY
        self.colorFill = "white"
        self.list = True
        self.value = False
        self.type = "passage"

    def GetColNr(self):
        return int(self.xStart / self.stepX)

    def GetRowNr(self):
        return int(self.yStart / self.stepY)

    def DrawGridPoint(self, image):
        draw = ImageDraw.Draw(image)
        if self.type == "passage":
            drawColor = "white"
        else:
            drawColor = "black"
        draw.rectangle(((self.xStart, self.yStart), (self.xEnd, self.yEnd)), fill=drawColor, outline=drawColor)
        del draw
        return
