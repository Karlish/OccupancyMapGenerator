# Occupancy Grid Map Generator

Outputs PNG images that can be used as a dataset for testing path planning algorithms.

Requirements:   
Python 3.6  
PIL

#### Usage
```
python MapGenerator.py
```

```
python MapGenerator.py -output_directory ./output -output_map_count 10
```

For other arguments
```
python MapGenerator.py -help
```

##### TODO
- [ ] Wider maze corridors 
- [ ] Refactor string constants